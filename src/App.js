import React, {Component} from 'react';
import './App.css';
import Game from "./snake_game/game";

class App extends Component {
    render() {
        return (
            <Game/>
        );
    }
}

export default App;
